source 'sandbox.m'

camera_count = 2;
points_count = 200;

P = zeros(camera_count * 3, 4);
P(1:3, :) = make_camera(0, [0; 0; 0]);
P(4:6, :) = make_camera(-pi / 3, [-3; 0; 1]);

X = generate_points(points_count);
Xt = generate_points(points_count);
[x depths] = project_points(X, P, 0.0);
[xt _] = project_points(Xt, P, 0.0);

F = compute_F(x(1:3, :), x(4:6, :));
Pc = compute_P(F, x(1:3, 1), x(4:6, 1));
Pc = [eye(3, 4); Pc]

[_ depths_c] = triangulate_all_points(Pc, x);
[Pcc Xcc errors] = improve_calibration(depths_c, x);

plot(errors);

% no four of these points are coplanar
Xcs = [-1 0 2 1;
        1 0 2 1;
        0 1 2 1;
        0 0 3 1;
        0 1 3 1]';
[xcs _] = project_points(Xcs, P, 0.0);
[Xcst _] = triangulate_all_points(Pcc, xcs);
H1 = compute_H(Xcs, Xcst);

Xcs = [ 0 0 2 1;
        1 0 2 1;
        0 1 2 1;
        0 0 3 1]';
[xcs _] = project_points(Xcs, P, 0.0);
[Xcst _] = triangulate_all_points(Pcc, xcs);
H2 = compute_H_2(Xcs, Xcst, Pcc(1:3, :))

k = H1(1,1) / H2(1,1);
H1 /= k;

H1
H2

Pcc1 = Pcc * inv(H1)
Pcc2 = Pcc * inv(H2)

[Xtt0 _] = triangulate_all_points(Pc, xt);
[Xtt1 _] = triangulate_all_points(Pcc1, xt);
[Xtt2 _] = triangulate_all_points(Pcc2, xt);
[xtt0 _] = project_points(Xtt0, Pc, 0);
[xtt1 _] = project_points(Xtt1, Pcc1, 0);
[xtt2 _] = project_points(Xtt2, Pcc2, 0);

error0 = RMS(xt - xtt0)
error1 = RMS(xt - xtt1)
error2 = RMS(xt - xtt2)

error0 / error1
error0 / error2
