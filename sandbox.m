function P = make_camera(angle, c)
  R = [ cos(angle) 0  sin(angle);
        0          1  0;
       -sin(angle) 0  cos(angle)];
  t = -R * c;

  P = [R t];
end

function X = generate_points(points_count)
  % generation 3D points
  X = ones(4, points_count);
  X(1:3, :) = (rand(3, points_count) - ones(3, points_count) * 0.5) * 2;
  X(3, :) += 3;
end

function [x, depths] = project_points(X, P, coef)
  x = P * X;
  depths = x(3:3:end, :);
  % projecting
  x ./= kron(depths, ones(3, 1));
  % noise adding
  x += (rand(size(x)) * 2.0 - ones(size(x))) * coef * 0.5;
  x(3:3:end, :) = 1;
end

function [X, depths] = triangulate(P, x)
  camera_count = rows(P) / 3;
  %     | R1 x1 0 0 0 |     | -t1 |
  %     | R2 0 x2 0 0 |     | -t2 |
  % A = | R3 0 0 x3 0 | b = | -t3 |
  %     | R4 0 0 0 x4 |     | -t4 |
  A = zeros(3 * camera_count, 3 + camera_count);
  A(:, 1:3) = P(:, 1:3);

  for i = 1:camera_count
    A(i * 3 - 2:i * 3, i + 3) = -x(i * 3 - 2:i * 3);
  endfor

  b = -P(:, 4);

  %     |  X  |
  %     |  Y  |
  %     |  Z  |
  % c = |  w1 |
  %     |  w2 |
  %     |  w3 |
  %     |  w4 |
  c = pinv(A) * b;

  X = [c(1:3); 1];
  depths = c(4:4 + camera_count - 1);
end

function [X, depths] = triangulate_all_points(P, x)
  camera_count = rows(P) / 3;
  points_count = columns(x);

  X = [];
  depths = [];
  for i = 1:points_count
    [point w] = triangulate(P, x(:, i));
    X = [X point];
    depths = [depths w];
  end
end

function N = compute_normalization_matrix(x)
  points_count = columns(x);

  center = mean(x')';
  delta_x = x - repmat(center, 1, points_count);
  scale = sqrt(2.0 / mean(sqrt(dot(delta_x, delta_x))));

  N = [scale 0 -center(1) * scale;
       0 scale -center(2) * scale;
       0 0 1];
end

function F = compute_F(x1, x2)
  points_count = columns(x1);

  T1 = compute_normalization_matrix(x1);
  T2 = compute_normalization_matrix(x2);

  x1 = T1 * x1;
  x2 = T2 * x2;

  % preparation for a solve
  A = zeros(points_count, 9);
  for i = 1:points_count
    A(i, 1) = x2(1, i) * x1(1, i);
    A(i, 2) = x2(1, i) * x1(2, i);
    A(i, 3) = x2(1, i);
    A(i, 4) = x2(2, i) * x1(1, i);
    A(i, 5) = x2(2, i) * x1(2, i);
    A(i, 6) = x2(2, i);
    A(i, 7) = x1(1, i);
    A(i, 8) = x1(2, i);
    A(i, 9) = 1;
  end

  % least squares solution
  [U S V] = svd(A);
  F = reshape(V(:, 9), 3, 3)';

  % constraint enforcement, get the closest rank 2 matrix
  [U S V] = svd(F);
  S(3, 3) = 0;
  F = U * S * V';

  F = T2' * F * T1;
end

function Result = compute_P(F, x1, x2)
  % calculation R and t
  [U S V] = svd(F);
  W = zeros(3, 3);
  W(1, 2) = -1;
  W(2, 1) =  1;
  W(3, 3) =  1;

  % two variants of the `R`, up to a sign
  R = cell(2, 1);
  R{1} = U * W  * V';
  R{2} = U * W' * V';

  % two variants of the `t`, up to a scale
  t = cell(2, 1);
  t{1} =  U * [0; 0; 1];
  t{2} = -U * [0; 0; 1];

  P = cell(4, 1);
  P{1} = [R{1} t{1}] / det(R{1});
  P{2} = [R{1} t{2}] / det(R{1});
  P{3} = [R{2} t{1}] / det(R{2});
  P{4} = [R{2} t{2}] / det(R{2});

  for i = 1:4
    [X1 depths] = triangulate([eye(3, 4); P{i}], [x1; x2]);
    X2 = P{i} * X1;
    if (X1(3) > 0 && X2(3) > 0)
      Result = P{i};
      break;
    end
  end
end

function normalized_depths = normalize_depths(depths)
  for i = 1:10
    depths ./= repmat(sqrt(dot(depths', depths'))', 1, columns(depths));
    depths ./= repmat(sqrt(dot(depths, depths)), rows(depths), 1);
  end

  normalized_depths = depths;
end

function [P, X, errors] = improve_calibration(depths, x)
  camera_count = rows(depths);
  N = zeros(camera_count * 3, camera_count * 3);
  for i = 1:camera_count
    N(i*3-2:i*3, i*3-2:i*3) = compute_normalization_matrix(x(i*3-2:i*3, :));
  end

  xn = N * x;
  errors = [];
  i = 0;
  while (true)
    depths = normalize_depths(depths);
    W = kron(depths, ones(3, 1)) .* xn;

    [U S V] = svd(W);
    P = U(:, 1:4) * S(1:4, 1:4);
    X = V(:, 1:4)';

    [_ depths] = project_points(X, P, 0);

    if (i > 1)
      error = norm(W - prev_W);

      if (i > 2 && abs(error - errors(end)) < 1e-6)
        break
      end

      errors = [errors; error];
    end

    i += 1;
    prev_W = W;
  end

  P = inv(N) * P;
end

function H = compute_H(euclidean_X, projective_X)
  points_count = 5;

  A = zeros(points_count * 4, 16 + points_count);
  for j = 1:points_count
    for i = 1:4
      A(i+(j-1)*4, i*4-3:i*4) = projective_X(:, j)';
    end
    A(j*4-3:j*4, j+16) = euclidean_X(:, j);
  end
  [U S V] = svd(A);
  H = reshape(V(1:16, 21), 4, 4)';
end

function H = compute_H_2(euclidean_X, projective_X, P)
  points_count = 4;

  H = [P; 0 0 0 0];

  A = zeros(16, 8);
  b = zeros(16, 1);
  for i = 1:points_count
    A(i*4, 1:4) = projective_X(:, i);
    A(i*4-3:i*4, 4+i) = euclidean_X(:, i);

    b(i*4-3:i*4-1) = -H(1:3, :) * projective_X(:, i);
  end

  v = pinv(A) * b;
  H(4, :) = v(1:4)';
end

function err = RMS(x)
  err = sqrt(mean(dot(x, x)));
end
