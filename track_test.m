source('sandbox.m');

function s = skm(v)
  s = [0    -v(3) v(2);
       v(3)    0 -v(1);
      -v(2)  v(1)   0];
end

function R = rotm(t, f)
  v = cross(f,t);
  s = norm(v);
  c = dot(f,t);
  if s == 0
    R = eye(3) + skm(v);
  else
    R = eye(3) + skm(v) + skm(v)*skm(v)*(1-c)/s^2;
  end
end

function P = make_camera2(p, c)
  z0 = [0; 0; 1];
  z1 = (c-p)/norm(c-p);
  R = rotm(z0, z1);

  t = -R*p;
  P = [R t];
end

function K = make_K(sx, sy, cx, cy)
  K =  [ sx 0  cx
         0  sy cy
         0  0  1 ];
end

function p = pellipse(c, t)
  w = 1.0;
  p = c(1,:)' + c(2,:)'*cos(w*t) + c(3,:)'*sin(w*t);
end

K = make_K(320, 320, 320, 240);
P0 = K*make_camera2([ 3;-3;-3], [0;0;0]);
P1 = K*make_camera2([-3; 3;-3], [0;0;0]);
P2 = K*make_camera2([ 3; 3;-3], [0;0;0]);
P3 = K*make_camera2([-3;-3; 3], [0;0;0]);
save -ascii Pc P0 P1 P2 P3

ell = [0 0 0; 2 0 0; 0 2 2];
#c = pellipse(ell, linspace(0,10,100))';
#save -ascii points c

function tr = make_track(ell, t)
  c = pellipse(ell, t);
  tr = [c; repmat([1], 1, 100)];
end

function ctr = make_cam_track(ell, P, t)
  tr = make_track(ell, t);
  ctr = round(project_points(tr, P, 0));
  ctr = [ctr; t];
end

t_sync = linspace(0,10,100);
tr_s = make_track(ell, t_sync);
tr2_s = make_cam_track(ell, [P0;P1;P2;P3], t_sync)(1:end-1, :);
[tr3_s, _] = triangulate_all_points([P0;P1;P2;P3], tr2_s);
[tr2_sp, _] = project_points(tr3_s, [P0;P1;P2;P3], 0);
err = RMS(tr2_sp - tr2_s)

t0 = linspace(0.1,10.1,100);
t1 = linspace(0.2,10.2,100);
t2 = linspace(0.3,10.3,100);
t3 = linspace(0.4,10.4,100);

tr2_0 = make_cam_track(ell, P0, t0)(1:3,:);
tr2_1 = make_cam_track(ell, P1, t1)(1:3,:);
tr2_2 = make_cam_track(ell, P2, t2)(1:3,:);
tr2_3 = make_cam_track(ell, P3, t3)(1:3,:);

[tr3_us, _] = triangulate_all_points([P0;P1;P2;P3], [tr2_0; tr2_1; tr2_2; tr2_3]);
[tr2_us, _] = project_points(tr3_us, [P0;P1;P2;P3], 0);
err = RMS(tr2_us - tr2_s)

tr2_0 = make_cam_track(ell, P0, t0);
tr2_1 = make_cam_track(ell, P1, t1);
tr2_2 = make_cam_track(ell, P2, t2);
tr2_3 = make_cam_track(ell, P3, t3);

tr2_0s = interp1( tr2_0(4,:)', tr2_0(1:3,:)', t_sync', 'cubic', 'extrap');
tr2_1s = interp1( tr2_1(4,:)', tr2_1(1:3,:)', t_sync', 'cubic', 'extrap');
tr2_2s = interp1( tr2_2(4,:)', tr2_2(1:3,:)', t_sync', 'cubic', 'extrap');
tr2_3s = interp1( tr2_3(4,:)', tr2_3(1:3,:)', t_sync', 'cubic', 'extrap');

[tr3_us, _] = triangulate_all_points([P0;P1;P2;P3], [tr2_0s'; tr2_1s'; tr2_2s'; tr2_3s']);
[tr2_us, _] = project_points(tr3_us, [P0;P1;P2;P3], 0);
err = RMS(tr2_us - tr2_s)
